﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public GameObject Owl;
	public float Distance = 5;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float x = Owl.transform.position.x > 2f ? Owl.transform.position.x : 2f;
		this.transform.position = new Vector3(x, Owl.transform.position.y, -Distance);
	}
}
