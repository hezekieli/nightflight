﻿using UnityEngine;
using System.Collections;

public class OwlControl : MonoBehaviour {

	public GameObject Wings;
	private Rigidbody2D rb2d;
	private bool mouseDown = false;
	
	private Vector2 flapStrength = new Vector2(400f, 1500f);
	private Vector2 glideStrength = new Vector2 (0f, 70f);
	private float flapDuration = 0.3f;
	private float flapProgress = 0f;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
		// RELOAD SCENE
		if (Input.GetKeyDown(KeyCode.R)) {
			Application.LoadLevel(Application.loadedLevel);
		}

		if (!mouseDown) {
			if (Input.GetMouseButtonDown (0)) {
				//Debug.Log ("Mouse Down");
				//Debug.Log ("DeltaTime = " + Time.deltaTime);
				mouseDown = true;
				flapProgress = flapDuration;
			}
		}
		else {
			if (Input.GetMouseButtonUp (0)) {
				//Debug.Log ("Mouse Up");
				mouseDown = false;
			}
		}

		float v = Vector2.Distance(new Vector2(0f, 0f), rb2d.velocity);

		if (flapProgress > 0) {
			Vector2 deltaStrength = flapStrength * Time.deltaTime;
			float flapStrengthProgress = (flapDuration - flapProgress) / flapDuration;

			//Debug.Log("Delta Strength = " + deltaStrength);

			//rb2d.AddForceAtPosition (new Vector2 (Mathf.Abs (-flapStrengthProgress * 1 * deltaStrength), Mathf.Abs (-flapStrengthProgress * 6 * deltaStrength)), new Vector2 (0f, 0.1f));
			rb2d.AddForceAtPosition (Mathf.Abs (-flapStrengthProgress) * deltaStrength, new Vector2 (0f, 0.1f));


			float scaleY = Mathf.Sin (2 * Mathf.PI * flapStrengthProgress);
			//Debug.Log ("scaleY = " + scaleY);
			Wings.transform.localScale = new Vector3 (1, scaleY, 1);

			flapProgress = (flapProgress - Time.deltaTime) < 0 ? 0 : flapProgress - Time.deltaTime;

		} 
		else if (mouseDown) {
			Debug.Log ("Mouse Down");
			rb2d.AddForceAtPosition (rb2d.velocity.x * glideStrength * Time.deltaTime, new Vector2 (0f, 0.1f));

			Wings.transform.localScale = new Vector3 (1, -0.2f, 1);
		}
		else {
			Wings.transform.localScale = new Vector3 (0.5f, -0.2f, 1);
		}

		if (v > 0.5f) 
			this.transform.localEulerAngles = new Vector3(0f, 0f, Mathf.Atan2(rb2d.velocity.y,rb2d.velocity.x) * Mathf.Rad2Deg);


	}
}
